-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 21-11-2022 a las 13:20:14
-- Versión del servidor: 5.7.33
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba_login`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `id` int(11) UNSIGNED NOT NULL,
  `cedula` int(11) NOT NULL,
  `rsponse` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`id`, `cedula`, `rsponse`) VALUES
(1, 123456789, 'null'),
(2, 987654321, 'null'),
(3, 12345, '{\"cliente\": {\"cliente_id\": 70765765, \"identificacion\": \"3614582\", \"nombrecompleto\": \"DARIO GUTIERREZ GALVIS\"}, \"listado_servicios\": {\"111\": \"CONTABILIDAD\", \"119\": \"SOPORTE\"}}'),
(4, 54321, '{\"cliente\": {\"cliente_id\": 70765765, \"identificacion\": \"3614582\", \"nombrecompleto\": \"DARIO GUTIERREZ GALVIS\"}, \"listado_menus\": {\"1\": \"Estado de cuenta\", \"2\": \"Pago de Mora\", \"3\": \"Contratos Afianzados\", \"4\": \"Estudio Arrendamiento\"}}'),
(5, 123456, '{\"cliente\": {\"cliente_id\": 89097654, \"identificacion\": \"16932023\", \"nombrecompleto\": \"PEDRO SANCHEZ\"}, \"listado_menus\": {\"1\": \"Estado de cuenta\", \"2\": \"Pagar\", \"3\": \"ya Realice Pago\", \"4\": \"Compromiso de Pago\"}}');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
